<?php

declare(strict_types=1);

namespace HelloAsso\HelloAssoGatewayPlugin\Payum;

use HelloAsso\HelloAssoGatewayPlugin\Payum\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class SyliusPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => 'hello_asso_payment',
            'payum.factory_title' => 'Hello Asso',
            'payum.action.status' => new StatusAction(),
        ]);

        $config['payum.api'] = function (ArrayObject $config) {
            return new HelloAssoApi(
                $config['hello_asso_id'],
                $config['hello_asso_secret'],
                $config['hello_asso_organization_slug']
            );
        };
    }
}
