<?php

declare(strict_types=1);

namespace HelloAsso\HelloAssoGatewayPlugin\Payum;

final class HelloAssoApi
{
    /** @var string */
    private $hello_asso_id;
    /** @var string */
    private $hello_asso_secret;
    /** @var string */
    private $hello_asso_organization_slug;

    public function __construct(string $hello_asso_id, string $hello_asso_secret, string $hello_asso_organization_slug)
    {
        $this->hello_asso_id = $hello_asso_id;
        $this->hello_asso_secret = $hello_asso_secret;
        $this->hello_asso_organization_slug = $hello_asso_organization_slug;
    }

    public function getHelloAssoId(): string
    {
        return $this->hello_asso_id;
    }

    public function getHelloAssoSecret(): string
    {
        return $this->hello_asso_secret;
    }

    public function getHelloAssoOrganizationSlug(): string
    {
        return $this->hello_asso_organization_slug;
    }


}
