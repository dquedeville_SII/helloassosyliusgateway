<?php

declare(strict_types=1);

namespace HelloAsso\HelloAssoGatewayPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class HelloAssoGatewayConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('hello_asso_id', TextType::class);
        $builder->add('hello_asso_secret', TextType::class);
        $builder->add('hello_asso_organization_slug', TextType::class);
    }
}
